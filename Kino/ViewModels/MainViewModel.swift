//
//  MainViewModel.swift
//  Kino
//
//  Created by Davron Usmanov on 28/02/22.
//

import Foundation
import Combine

class MainViewModel: ObservableObject {
    
    @Published var selectedSort: MovieType = .Popular
    
    //MARK: Data Models
    @Published var popularMovies : PopularMovies = []
    @Published var topRatedMovies: TopRatedMovies = []
    @Published var upcomingMovies: UpcomingMovies = []
    
    
    let dataService = MainDataService()
    private var cancellables = Set<AnyCancellable>()
    
    
    init() {
        subscribers()
    }
    
    
    private func subscribers() {
        
        dataService.$popularMovies
            .subscribe(on: DispatchQueue.global(qos: .background))
            .receive(on: DispatchQueue.main)
            .sink { [weak self] popularFilms in
                self?.popularMovies = popularFilms
            }
            .store(in: &cancellables)
        
        dataService.$topRatedMovies
            .subscribe(on: DispatchQueue.global(qos: .background))
            .receive(on: DispatchQueue.main)
            .sink { [weak self] topFilms in
                self?.topRatedMovies = topFilms
            }
            .store(in: &cancellables)
        
        dataService.$upcomingMovies
            .subscribe(on: DispatchQueue.global(qos: .background))
            .receive(on: DispatchQueue.main)
            .sink { [weak self] upcomingFilms in
                self?.upcomingMovies = upcomingFilms
            }
            .store(in: &cancellables)
        
    }
    
    func getMovieList(by movieType: MovieType) {
        dataService.getMovieList(by: movieType)
    }
    
}
