//
//  MovieDetailViewModel.swift
//  Kino
//
//  Created by Davron Usmanov on 28/02/22.
//

import Foundation
import Combine

class MovieDetailViewModel: ObservableObject {
 
    @Published var movie: MovieDetail? = nil
    @Published var actorList: ActorList = []
    @Published var actorDetail: ActorDetail? = nil
    @Published var actorMovies: ActorMovies = []
    
    
    let dataService = MainDataService()
    private var cancellables = Set<AnyCancellable>()
    
    init(id: Int) {
        subscribers()
        getDetail(byID: id)
    }
    
    private func subscribers() {
        
        dataService.$movieDetail
            .subscribe(on: DispatchQueue.global(qos: .background))
            .receive(on: DispatchQueue.main)
            .sink { [weak self] detailModel in
                self?.movie = detailModel
                
                if let model = detailModel {
                    self?.dataService.getActors(movie_id: model.id ?? 0)
                }
            }
            .store(in: &cancellables)
        
        dataService.$actorsList
            .subscribe(on: DispatchQueue.global(qos: .background))
            .receive(on: DispatchQueue.main)
            .sink { [weak self] actorModels in
                self?.actorList = actorModels
            }
            .store(in: &cancellables)
        
        dataService.$actorDetail
            .subscribe(on: DispatchQueue.global(qos: .background))
            .receive(on: DispatchQueue.main)
            .sink { [weak self] actorDetail in
                self?.actorDetail = actorDetail
                
                if let actorDetail = actorDetail {
                    self?.dataService.getMovies(byActorID: actorDetail.id ?? 0)
                }
            }
            .store(in: &cancellables)
        
        dataService.$actorMovies
            .subscribe(on: DispatchQueue.global(qos: .background))
            .receive(on: DispatchQueue.main)
            .sink { [weak self] actorDetail in
                self?.actorMovies = actorDetail
            }
            .store(in: &cancellables)
    }
    
    func getDetail(byID id: Int) {
        dataService.getMovieDetail(byID: id)
    }
    
    func getActorDetail(byID id: Int) {
        dataService.getActorDetail(actor_ID: id)
    }
    
}
