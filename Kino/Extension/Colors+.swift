//
//  Colors.swift
//  Kino
//
//  Created by Davron Usmanov on 28/02/22.
//

import Foundation
import SwiftUI

extension Color {
    static let themeColor = ThemeColor()
}

struct ThemeColor {
    let accent = Color("asset")
    let main_bg = Color("main_bg")
    let asset_second = Color("asset_second")
}
