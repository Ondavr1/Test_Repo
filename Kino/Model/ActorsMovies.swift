//
//  ActorsMovies.swift
//  Kino
//
//  Created by Davron Usmanov on 27/02/22.
//

import Foundation

// MARK: - ActorsMovies
struct ActorsMovies: Codable {
    let cast, crew: [ActorCast]?
    let id: Int?
}

typealias ActorMovies = [ActorCast]

// MARK: - Cast
struct ActorCast: Codable {
    let video: Bool?
    let voteAverage: Double?
    let title, overview, releaseDate: String?
    let id: Int?
    let adult: Bool?
    let backdropPath: String?
    let genreIDS: [Int]?
    let originalLanguage: String?
    let originalTitle: String?
    let posterPath: String?
    let voteCount: Int?
    let popularity: Double?
    let character, creditID: String?
    let order: Int?
    let department, job: String?

    enum CodingKeys: String, CodingKey {
        case video
        case voteAverage = "vote_average"
        case title, overview
        case releaseDate = "release_date"
        case id, adult
        case backdropPath = "backdrop_path"
        case genreIDS = "genre_ids"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case posterPath = "poster_path"
        case voteCount = "vote_count"
        case popularity, character
        case creditID = "credit_id"
        case order, department, job
    }
}

