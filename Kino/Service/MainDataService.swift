//
//  MainDataService.swift
//  Kino
//
//  Created by Davron Usmanov on 26/02/22.
//

import Foundation
import Combine

class MainDataService {
    
    //MARK: MainView Service PROPERTIES
    @Published var popularMovies : PopularMovies = []
    @Published var topRatedMovies: TopRatedMovies = []
    @Published var upcomingMovies: UpcomingMovies = []
    
    //MARK: MovieDetail view PROPERTIES
    @Published var movieDetail: MovieDetail? = nil
    @Published var actorsList: ActorList = []
    
    //MARK: ActorDetail View PROPERTIES
    @Published var actorDetail: ActorDetail? = nil
    @Published var actorMovies: ActorMovies = []
    
    var cancellable: AnyCancellable?
    var network = NetworkManager()
    
    init() {}
    
    
    
    func getMovieList(by movieType: MovieType) {
        
        var listType = ""
        
        switch movieType {
        case .Popular:
            listType = "popular"
        case .Top:
            listType = "top_rated"
        case .Upcoming:
            listType = "upcoming"
        }
        
        
        cancellable = network.getData(url: "movie/\(listType)?api_key=\(NetworkManager.api_key)&language=en-US&page=1")
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: network.handleCompletion, receiveValue: { [weak self] (movies) in
                guard let self = self else { return }
                
                switch movieType {
                case .Popular:
                    
                    let model = try? JSONDecoder().decode(PopularMovie.self, from: movies)
                    self.popularMovies = model?.results ?? []
                    
                case .Top:
                    let model = try? JSONDecoder().decode(TopRatedMovie.self, from: movies)
                    self.topRatedMovies = model?.results ?? []
                    
                case .Upcoming:
                    let model = try? JSONDecoder().decode(UpcomingMovie.self, from: movies)
                    self.upcomingMovies = model?.results ?? []
                }
                
                
                self.cancellable?.cancel()
            })
    }
    
    func getMovieDetail(byID id: Int) {
        
        cancellable = network.getData(url: "movie/\(id)?api_key=\(NetworkManager.api_key)&language=en-US")
            .receive(on: DispatchQueue.main)
            .decode(type: MovieDetail.self, decoder: JSONDecoder())
            .sink(receiveCompletion: network.handleCompletion, receiveValue: { [weak self] model in
                self?.movieDetail = model
                self?.cancellable?.cancel()
            })
    }
    
    func getActors(movie_id: Int) {
        cancellable = network.getData(url: "movie/\(movie_id)/credits?api_key=\(NetworkManager.api_key)&language=en-US")
            .receive(on: DispatchQueue.main)
            .decode(type: ActorsModel.self, decoder: JSONDecoder())
            .sink(receiveCompletion: network.handleCompletion, receiveValue: { [weak self] actors in
                self?.actorsList = actors.cast ?? []
                self?.cancellable?.cancel()
            })
    }
    
    func getActorDetail(actor_ID: Int) {
        cancellable = network.getData(url: "/person/\(actor_ID)?api_key=\(NetworkManager.api_key)&language=en-US")
            .receive(on: DispatchQueue.main)
            .decode(type: ActorDetail.self, decoder: JSONDecoder())
            .sink(receiveCompletion: network.handleCompletion, receiveValue: { [weak self] actors in
                self?.actorDetail = actors
                self?.cancellable?.cancel()
            })
    }
    
    func getMovies(byActorID id: Int) {
        cancellable = network.getData(url: "person/\(id)/movie_credits?api_key=\(NetworkManager.api_key)&language=en-US")
            .receive(on: DispatchQueue.main)
            .decode(type: ActorsMovies.self, decoder: JSONDecoder())
            .sink(receiveCompletion: network.handleCompletion, receiveValue: { [weak self] actorsMovies in
                self?.actorMovies = actorsMovies.cast ?? []
                self?.cancellable?.cancel()
            })
    }
}

enum MovieType: String {
    case Popular, Top, Upcoming
    
    var name: String {
        switch self {
        case .Popular:  return "Popular"
        case .Top:      return "Top"
        case .Upcoming: return "Upcoming"
        }
    }
}
