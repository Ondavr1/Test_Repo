//
//  SingleMovieView.swift
//  Kino
//
//  Created by Davron Usmanov on 26/02/22.
//

import SwiftUI
import Kingfisher

struct SingleMovieView: View {
    
    var imageUrlStr: String
    var movieTitle:  String
    
    var body: some View {
        VStack(alignment: .center, spacing: 12) {

            KFImage.url(URL(string: NetworkManager.image_url + imageUrlStr))
//                      .placeholder(placeholderImage)
//                      .setProcessor(processor)
                      .loadDiskFileSynchronously()
                      .cacheMemoryOnly()
                      .fade(duration: 0.25)
//                      .lowDataModeSource(.network(lowResolutionURL))
                      .onProgress { receivedSize, totalSize in  }
                      .onSuccess { result in  }
                      .onFailure { error in }
                      .resizable()
                      .scaledToFit()
                      .cornerRadius(16)
                      .frame(width: 145, height: 160)
                      
                
            Text(movieTitle)
                .foregroundColor(.white)
                .font(.system(size: 14))
                .padding(.leading, 10)
        }
        .frame(width: 145)
    }
}

struct SingleMovieView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
