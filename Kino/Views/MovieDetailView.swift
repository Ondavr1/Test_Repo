//
//  MovieDetailView.swift
//  Kino
//
//  Created by Davron Usmanov on 26/02/22.
//

import SwiftUI
import Kingfisher

struct MovieDetailView: View {
    
    @StateObject var viewModel: MovieDetailViewModel
    
    init(id: Int) {
        self._viewModel = StateObject(wrappedValue: MovieDetailViewModel(id: id))
    }
    
    @State var showView = false
    @State var selectedActorID = 0
    var model: MovieDetail? { viewModel.movie  }
    var imageUrl: String { NetworkManager.image_url + (viewModel.movie?.posterPath ?? "")  }
    
    var body: some View {
        
        ScrollView(.vertical, showsIndicators: false, content: {
            VStack(spacing: 0) {
                
                KFImage.url(URL(string: imageUrl))
    //                      .placeholder(placeholderImage)
    //                      .setProcessor(processor)
                          .loadDiskFileSynchronously()
                          .cacheMemoryOnly()
                          .fade(duration: 0.25)
    //                      .lowDataModeSource(.network(lowResolutionURL))
                          .onProgress { receivedSize, totalSize in  }
                          .onSuccess { result in  }
                          .onFailure { error in }
                          .resizable()
                          .scaledToFill()
                          .cornerRadius(12)
                          .frame(width: width)
                          .ignoresSafeArea()
                    
                    
                VStack(alignment: .leading, spacing: 0) {
                    
                    Text(model?.title ?? "")
                        .foregroundColor(.white)
                        .fontWeight(.medium)
                        .font(.system(size: 24))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.top, 28)
                    
                    HStack {
                        
                        Image(systemName: "stopwatch")
                        
                        Text("\(model?.runtime ?? 0) minutes")
                        
                        Image(systemName: "star.fill")
                        
                        Text("\(model?.popularity ?? 0.0) (IMDb)")
                    }
                    .foregroundColor(.gray)
                    .padding(.top, 10)
                    
                    Divider()
                        .background(.gray)
                        .padding(.vertical, 18)
                    
                    
                    //MARK: Release Date
                    releaseSubView
                    
                    Divider()
                        .background(.gray)
                        .padding(.vertical, 18)
                    
                    //MARK: Description
                    descriptionSubView
                    
                }
                .padding(.horizontal, 20)
                .frame(maxWidth: .infinity)
                
                
                VStack(alignment: .leading, spacing: 20) {
                    Text("Acters")
                        .fontWeight(.medium)
                        .font(.system(size: 18))
                        .foregroundColor(.white)
                        .padding(.leading, 20)
                    
                    ScrollView(.horizontal, showsIndicators: false) {
                        
                        HStack(spacing: 20) {
                            ForEach((0..<viewModel.actorList.count), id: \.self) { index in

                                Button {
                                    selectedActorID = viewModel.actorList[index].id ?? 0
                                    showView = true
                                } label: {
                                    SingleMovieView(
                                        imageUrlStr: viewModel.actorList[index].profilePath ?? "",
                                        movieTitle: viewModel.actorList[index].name ?? "")
                                }
                                
                            }
                        }
                        .padding(.horizontal, 20)
                        
                    }
                }
                .padding(.vertical, 20)
                
            }
            
            if showView {
                NavigationLink(destination: ActorDetailView(actorID: selectedActorID).environmentObject(viewModel), isActive: $showView) {}
            }
        })
        .frame(width: width, alignment: .top)
        .background(Color.themeColor.main_bg)
        
        
    }
}

extension MovieDetailView {
    
    private var releaseSubView: some View {
        HStack(spacing: 60) {
            
            VStack(alignment: .leading, spacing: 14) {
                Text("Release date")
                    .foregroundColor(.white)
                    .font(.system(size: 18))
                    .fontWeight(.medium)
                
                Text(model?.releaseDate ?? "")
                    .foregroundColor(.gray)
                    .font(.system(size: 14))
            }
            
        }
    }
    
    private var descriptionSubView: some View {
        VStack(alignment:.leading, spacing: 14) {
            
            Text("Overview")
                .fontWeight(.medium)
                .font(.system(size: 19))
                .foregroundColor(.white)
            
            Text(model?.overview ?? "")
                .foregroundColor(.gray)
                .font(.system(size:14))
        }
    }
    
}

struct MovieDetailView_Previews: PreviewProvider {
    static var previews: some View {
        MovieDetailView(id: 0)
    }
}
