//
//  MainView.swift
//  Kino
//
//  Created by Davron Usmanov on 26/02/22.
//

import SwiftUI

struct MainView: View {
    
    @StateObject var viewModel = MainViewModel()
    @State var showView = false
    @State var selectedID = 0
    
    private let categoryNames = ["Popular", "Top Rated", "Upcoming"]
    private let categoryType: [MovieType] = [.Popular, .Top, .Upcoming]
    
    let columns = [
        GridItem(.flexible(), spacing: 0),
        GridItem(.flexible())
    ]
    
    @State var searchText = ""
    
    var body: some View {
        
        VStack {
            
            //NavBar Text
            Text("Find Movies, Tv series, and more..")
                .foregroundColor(.white)
                .fontWeight(.medium)
                .font(.system(size: 28))
                .frame(maxWidth: .infinity, maxHeight: 70, alignment: .leading)
                .padding(.top, 36)
            
            
            //SearchBar
            searchBar
            
            //TopBars
            topBarCategories
            
            //Movies List
            
            ScrollView(.vertical, showsIndicators: false) {
                getListOfMovies(by: viewModel.selectedSort)
                .padding(.top, 16)
                .padding(.bottom, 30)
            }
            .frame(maxWidth: .infinity)
            
            .onAppear {
                if viewModel.popularMovies.isEmpty {
                    viewModel.getMovieList(by: .Popular)
                }
            }
            .onChange(of: viewModel.selectedSort) { newValue in
                
                switch newValue {
                case .Popular:
                    if viewModel.popularMovies.isEmpty {
                        viewModel.getMovieList(by: .Popular)
                    }
                    
                case .Top:
                    if viewModel.topRatedMovies.isEmpty {
                        viewModel.getMovieList(by: .Top)
                    }
                    
                case .Upcoming:
                    if viewModel.upcomingMovies.isEmpty {
                        viewModel.getMovieList(by: .Upcoming)
                    }
                }
                
            }
            
            if showView {
                NavigationLink(destination: MovieDetailView(id: selectedID), isActive: $showView) {}
            }
            
        }
        
        .frame(width: width, height: height)
        .background(Color.themeColor.main_bg)
    }
}

extension MainView {
    
    private var searchBar: some View {
        HStack(spacing: 16) {
            
            Image(systemName: "magnifyingglass")
                .foregroundColor(.white)
                .frame(width: 24, height: 24)
            
            TextField("Sherlock Holmes", text: $searchText)
            
        }
        .padding(.horizontal, 20)
        .frame(maxWidth: .infinity, maxHeight: 48, alignment: .leading)
        .background(Color.themeColor.asset_second)
        .cornerRadius(20)
        .padding(.top, 20)
    }
    
    private var topBarCategories: some View {
        HStack(spacing: 20) {
            ForEach(categoryType, id: \.self) { type in
                
                Button {
                    withAnimation{
                        viewModel.selectedSort = type
                    }
                    
                } label: {
                    VStack(spacing: 8) {
                        Text(type.name)
                            .font(.system(size: 18))
                            .foregroundColor(.themeColor.accent)
                        
                        Divider()
                            .frame(height: 3)
                            .background(Color.themeColor.accent)
                            .cornerRadius(3)
                            .isHidden(viewModel.selectedSort != type)
                    }
                }
                
            }
            
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding(.top, 24)
    }
    
    private func getListOfMovies(by type: MovieType) -> some View {

        switch type {
        case .Popular:
            
            return LazyVGrid(columns: columns, spacing: 15) {
                ForEach((0..<viewModel.popularMovies.count), id: \.self) { index in
                    
                    Button {
                        selectedID = viewModel.popularMovies[index].id ?? 0
                        showView = true
                    } label: {
                        SingleMovieView(
                            imageUrlStr: viewModel.popularMovies[index].posterPath ?? "",
                            movieTitle: viewModel.popularMovies[index].title ?? ""
                        )
                    }
                    
                }
            }

        case .Top:
            
            return LazyVGrid(columns: columns, spacing: 15) {
                ForEach((0..<viewModel.topRatedMovies.count), id: \.self) { index in
                    
                    Button {
                        selectedID = viewModel.topRatedMovies[index].id ?? 0
                        showView = true
                    } label: {
                        SingleMovieView(
                            imageUrlStr: viewModel.topRatedMovies[index].posterPath ?? "",
                            movieTitle: viewModel.topRatedMovies[index].title ?? ""
                        )
                    }
                    
                }
            }

        case .Upcoming:
            
            return LazyVGrid(columns: columns, spacing: 15) {
                ForEach((0..<viewModel.upcomingMovies.count), id: \.self) { index in
                    
                    Button {
                        selectedID = viewModel.upcomingMovies[index].id ?? 0
                        showView = true
                    } label: {
                        SingleMovieView(
                            imageUrlStr: viewModel.upcomingMovies[index].posterPath ?? "",
                            movieTitle: viewModel.upcomingMovies[index].title ?? ""
                        )
                    }
                    
                }
            }
            
        }

    }
    
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
