//
//  ActorDetailView.swift
//  Kino
//
//  Created by Davron Usmanov on 26/02/22.
//

import SwiftUI
import Kingfisher

struct ActorDetailView: View {
    
    @EnvironmentObject var viewModel: MovieDetailViewModel
    @State var showView = false
    @State var selectedID = 0
    
    var model: ActorDetail? { viewModel.actorDetail }
    var imageUrl: String { NetworkManager.image_url + (viewModel.actorDetail?.profilePath ?? "")  }
    
    var actorID: Int
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            
            VStack(spacing: 0) {
                
                KFImage.url(URL(string: imageUrl))
    //                      .placeholder(placeholderImage)
    //                      .setProcessor(processor)
                          .loadDiskFileSynchronously()
                          .cacheMemoryOnly()
                          .fade(duration: 0.25)
                          .onProgress { receivedSize, totalSize in  }
                          .onSuccess { result in  }
                          .onFailure { error in }
                          .resizable()
                          .scaledToFill()
                          .cornerRadius(12)
                          .frame(width: width - 100)
                          .ignoresSafeArea()
                          .padding(.vertical,30)
                    
                
                VStack(alignment: .leading, spacing: 10) {
                    Text("Name: \(model?.name ?? "")")
                        .foregroundColor(.gray)
                    
                    Text("Birthday: \(model?.birthday ?? "")")
                        .foregroundColor(.gray)
                    
                    Text("Place of birth: \(model?.placeOfBirth ?? "")")
                        .foregroundColor(.gray)
                    
                    Text("Deathday: \(model?.deathday ?? "")")
                        .foregroundColor(.gray)
                        .isHidden(model?.deathday == nil)
                    
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.vertical)
                
                
                descriptionSubView
                    .padding(.top, 20)
                    
            }
            .padding(.top, 30)
            .padding(.horizontal, 20)
            
            VStack(alignment: .leading, spacing: 20) {
                Text("Films")
                    .fontWeight(.medium)
                    .font(.system(size: 18))
                    .foregroundColor(.white)
                    .padding(.leading, 20)
                
                ScrollView(.horizontal, showsIndicators: false) {
                    
                    HStack(spacing: 20) {
                        
                        ForEach((0..<viewModel.actorMovies.count), id: \.self) { index in

                            Button {
                                selectedID = viewModel.actorMovies[index].id ?? 0
                                showView = true
                            } label: {
                                SingleMovieView(
                                    imageUrlStr: viewModel.actorMovies[index].posterPath ?? "",
                                    movieTitle: viewModel.actorMovies[index].title ?? ""
                                )
                            }

                            
                        }
                        
                    }
                    .padding(.horizontal, 20)
                    
                }
            }
            .padding(.vertical, 20)
            .padding(.bottom, 60)
            
            if showView {
                NavigationLink(destination: MovieDetailView(id: selectedID), isActive: $showView) {}
            }
        }
        .frame(width: width, height: height)
        .background(Color.themeColor.main_bg)
        
        .onAppear {
            viewModel.getActorDetail(byID: actorID)
        }
        
    }
}

extension ActorDetailView {
    
    private var descriptionSubView: some View {
        VStack(alignment:.leading, spacing: 14) {
            
            Text("Biography")
                .fontWeight(.medium)
                .font(.system(size: 19))
                .foregroundColor(.white)
            
            Text(model?.biography ?? "")
                .foregroundColor(.gray)
                .font(.system(size:14))
        }
    }
    
}

struct ActorDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ActorDetailView(actorID: 0)
    }
}
