//
//  NetworkManager.swift
//  Kino
//
//  Created by Davron Usmanov on 27/02/22.
//

import Foundation
import Combine
import SwiftUI

class NetworkManager {
    
    private let base_url = "https://api.themoviedb.org/3/"
    static let api_key = "ea06e9896ef95b7e8e14994c770e2f24"
    static let image_url = "https://image.tmdb.org/t/p/w500"
    
    enum NetworkingError: LocalizedError {
        case badURLResponse(url: String)
        case status(status: Int)
        case unknown
        
        var errorDescription: String? {
            switch self {
            case .badURLResponse(url: let url): return "[🔥] Bad response from URL: \(url)"
            case .status(status: let status): return "[⚠️] Status Error -> [ \(status) ]"
            case .unknown: return "[⚠️] Unknown error occured"
            }
        }
    }
    
    
    func getData(url: String) -> AnyPublisher<Data ,Error> {
        
        guard let myUrl = URL(string: base_url + url) else {
            print("DAs")
            return Fail(error: NetworkingError.badURLResponse(url: url))
                .eraseToAnyPublisher()
        }
        
        print(myUrl)
        
        return URLSession.shared.dataTaskPublisher(for: myUrl)
            .tryMap { try self.handleUrlResponse(output: $0, url: myUrl) }
            .retry(3)
            .eraseToAnyPublisher()
    }
    
    func handleUrlResponse(
        output: URLSession.DataTaskPublisher.Output,
        url: URL
    ) throws -> Data {
        
        guard let response = output.response as? HTTPURLResponse, response.statusCode >= 200 && response.statusCode < 300
        
        else {
            throw NetworkingError.status(status: (output.response as? HTTPURLResponse)!.statusCode )
        }
        
        
        return output.data
    }
    
    func handleCompletion(completion: Subscribers.Completion<Error>) {
        switch completion {
        case .finished:
            print(#function, "Finished")
            break
        case .failure(let error):
            
            print(#function, "Failed error-> \(error)")
         }
    }
    
}
