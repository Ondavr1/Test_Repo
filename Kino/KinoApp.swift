//
//  KinoApp.swift
//  Kino
//
//  Created by Davron Usmanov on 26/02/22.
//

import SwiftUI

@main
struct KinoApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                NavigationView {
                    MainView()
                        .navigationBarHidden(true)
                        .navigationBarBackButtonHidden(true)
                }
            }
        }
    }
}
